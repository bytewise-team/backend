package handler

import (
	"net/http"
	"io"
)

func RootHandler(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "This is the root directory.")
}