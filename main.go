// Package main provides an entry point into the server
package main

import "./server"

// Compile Time vars are passed in from the build system
var (
	// 	VERSION- The version of the build - also passed in at build
	VERSION = ""
	// 	BUILD - The type of build such as release, test or debug
	BUILD = ""
	// TIME - The datetime that the executable was built
	TIME = ""
)

func main() {
	server.StartServer(VERSION, BUILD, TIME)
}