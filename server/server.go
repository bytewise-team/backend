package server

import (
	"fmt"
	"net/http"
	"../handler"
)

const PORT string = "80"

type ServerPath struct {
	path string
	handler func(http.ResponseWriter, *http.Request)
}

func StartServer(version string, build string, time string) {
	fmt.Printf("Starting back-end web server v%s %s on localhost:%s\n",
		version, build, PORT)

	// Register all applicable paths and their handlers for the server
	s := []ServerPath{
		{
			path: "/",
			handler: handler.RootHandler,
		},
		{
			path: "/login",
			handler: handler.LoginHandler,
		},
		{
			path: "/register",
			handler: handler.RegisterHandler,
		},
	}

	// Start the web server and listen for requests
	for _, v := range s { http.HandleFunc(v.path, v.handler) }
	http.ListenAndServe(":" + PORT, nil)
}